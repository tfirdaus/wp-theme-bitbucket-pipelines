# WP CLI Bitbucket Pipelines (Theme)

An example of basic Bitbucket Pipelines configuration to run PHPUnit and PHPCS for a WordPress theme. This repository is aimed as a test to verify Bitbucket Pipelines configuration before being merged to [WP-CLI Scaffold Command](https://github.com/wp-cli/scaffold-command), providing `bitbucket` as a CI provider.

## References

- [Getting Started with Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html)
- [Configure bitbucket-pipelines.yml](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html)
- [Tips for scripting tasks with Bitbucket Pipelines](https://www.atlassian.com/continuous-delivery/tips-for-scripting-tasks-with-Bitbucket-Pipelines)
